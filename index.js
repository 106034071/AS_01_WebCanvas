var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');
var drawing = false; //是否正在畫畫
var startPos = [0, 0]; //開始畫東西的起始位置
var eraser = document.getElementById('eraser');
//var size = document.getElementById('can');
canvas.width = 1200;
canvas.height = 600;

//var drawColor = document.getElementById('color');
var penW = document.getElementById('penW');
ctx.strokeStyle = "#000000";
//console.log(drawColor.value);
ctx.lineWidth = penW.value;
ctx.lineJoin = 'round';
ctx.lineCap = 'round'; //畫筆形狀為圓頭
canvas.style.cursor = "url('pencil.png') 0 30, default";
var multicolor = -1;
var hue = 0;

var fontType = document.getElementById('font');
var fontSize = document.getElementById('fontS');
var textbox = document.getElementById('textbox');
ctx.font = fontSize.value * 5 + "px " + fontType.value;

var dataurl; //接toDataURL
var centerX = 0; //畫幾何圖形的起始固定點
var centerY = 0;
var mode = "pen"; //代表現在在畫哪種圖形，起始預設為畫筆

var StoreState = new Array();
var index = -1;
AddToState();

var can = document.getElementById("can");
can.style.marginLeft = (window.innerWidth - canvas.width) / 2 + "px";


var right = document.getElementById("right_side");
right.style.width = (window.innerWidth - canvas.getBoundingClientRect().right) + "px";

right.style.marginLeft = (canvas.getBoundingClientRect().right) + "px";
console.log(right.style.width);


var pickerblock = document.getElementById('pickerblock');
pickerblock.style.marginLeft = (right.offsetWidth - pickerblock.offsetWidth) / 2 + "px";
console.log(pickerblock.right);


var bb = document.getElementById("picker");
bb.style.marginTop = (pickerblock.offsetHeight - bb.offsetHeight) / 2 + "px";
console.log(bb.style.marginTop);

var ss = document.getElementById("slide");
ss.style.marginTop = (pickerblock.offsetHeight - ss.offsetHeight) / 2 + "px";
console.log(bb.style.marginTop);

textbox.addEventListener("keydown", keyEnter);

var havetextbox = false;

function keyEnter(e) {
  if (e.keyCode == 13) {
    ctx.fillText(textbox.value, textbox.getBoundingClientRect().left - canvas.getBoundingClientRect().left, textbox.getBoundingClientRect().top - canvas.getBoundingClientRect().top);
    textbox.style.display = "none";
    textbox.value = "";

  }
}


function changemode(m) {
  mode = m;
  ctx.globalCompositeOperation = "source-over";
  switch (m) {
    case 'eraser':
      ctx.globalCompositeOperation = "destination-out";
      canvas.style.cursor = "url('eraser.png') 0 30, default";
      break;
    case 'pen':
      canvas.style.cursor = "url('pencil.png')  0 30, default";
      break;
    case 'circle':
    case 'rect':
    case 'tri':
    case 'slash':
      canvas.style.cursor = "url('shape.png') 15 15, default";
      break;
    case 'text':
      canvas.style.cursor = "text";
      break;
    default:
      canvas.style.cursor = "url('pencil.png') 0 30, default";
      break;
  }
}

function colorChange() {
  multicolor *= -1;
  ctx.strokeStyle = "#000000";
}

ColorPicker(
  document.getElementById('slide'),
  document.getElementById('picker'),
  function (hex, hsv, rgb) {
    ctx.strokeStyle = hex;
    pickerblock.style.backgroundColor = hex;
  });

fontType.addEventListener('input', function () {
  ctx.font = fontSize.value * 5 + "px " + fontType.value;
  console.log(ctx.font);
});

fontSize.addEventListener('input', function () {
  ctx.font = fontSize.value * 5 + "px " + fontType.value;
  console.log(ctx.font);
})

penW.addEventListener('input', function () {
  ctx.lineWidth = penW.value;
});
canvas.addEventListener('mousedown', function (e) {
  drawing = true;
  startPos = [e.clientX - canvas.offsetLeft, e.clientY - canvas.offsetTop];
  centerX = startPos[0];
  centerY = startPos[1];
  dataurl = canvas.toDataURL();
  if(havetextbox)
  {
    textbox.style.display = "none";
  }
  if (mode == "text") {
    drawing = false;
    textbox.style.left = e.clientX + "px";
    textbox.style.top = e.clientY + "px";
    textbox.style.display = "block";
    havetextbox = true;
  }

});

canvas.addEventListener('mousemove', function (e) {
  console.log(drawing);
  if (drawing) {
    if (multicolor != -1) {
      ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`;
      hue++;
      if (hue == 360) {
        hue = 0;
      }
    }
    var newPos = [e.clientX - canvas.offsetLeft, e.clientY - canvas.offsetTop];
    if (mode == "eraser") {
      var eW = penW.value;
      ctx.beginPath();
      ctx.moveTo(startPos[0], startPos[1]);
      ctx.lineTo(newPos[0], newPos[1]);
      ctx.stroke();

    } else if (mode == "pen") {
      ctx.beginPath();
      ctx.moveTo(startPos[0], startPos[1]);
      ctx.lineTo(newPos[0], newPos[1]);
      ctx.stroke();

    } else if (mode == "circle") {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      loadURL();
      dx = Math.abs(newPos[0] - centerX);
      dy = Math.abs(newPos[1] - centerY);
      dis = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
      ctx.beginPath();
      ctx.arc(centerX, centerY, dis, 0, 2 * Math.PI);
      ctx.stroke();
    }
    else if (mode == 'rect') {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      loadURL();
      ctx.strokeRect(centerX, centerY, newPos[0] - centerX, newPos[1] - centerY);
    } else if (mode == 'slash') {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      loadURL();
      ctx.beginPath();
      ctx.moveTo(centerX, centerY);
      ctx.lineTo(newPos[0], newPos[1]);
      ctx.closePath();
      ctx.stroke();
    } else if (mode == 'tri') {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      loadURL();
      ctx.beginPath();
      ctx.moveTo(centerX, centerY);
      ctx.lineTo(newPos[0], newPos[1]);
      ctx.lineTo(centerX, newPos[1]);
      ctx.closePath();
      ctx.stroke();
    }
    startPos = newPos;
  }
});

canvas.addEventListener('mouseup', function () {
  drawing = false;
  AddToState();
});

function clearCanvas() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  mode = "pen";
  ctx.strokeStyle = "#000000";
  ctx.lineWidth = penW.value;
  canvas.style.cursor = "url('pencil.png'), default";
  AddToState();
}

function loadURL() {
  let img = new Image();
  img.src = dataurl;
  ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
}


function download() {
  var link = document.createElement('a');
  link.download = 'canvas.png';
  link.href = canvas.toDataURL("image/png");
  link.click();
}


var uploadimg = document.getElementById('myfile');
uploadimg.addEventListener('change', upload, false);

function upload(e) {
  var reader = new FileReader();
  reader.onload = function (event) {
    var img = new Image();
    img.onload = function () {
      ctx.drawImage(img, 0, 0);
      AddToState();
    }
    img.src = event.target.result;
  }
  reader.readAsDataURL(e.target.files[0]);
}


function AddToState() {
  index++;
  if (index < StoreState.length) {
    StoreState.length = index;
  }
  StoreState.push(canvas.toDataURL());
}

function undo() {
  if (index > 0) {
    index--;
    var img = new Image();
    img.src = StoreState[index];
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    img.onload = function () { ctx.drawImage(img, 0, 0); }
  }
}
function redo() {
  if (index < StoreState.length - 1) {
    index++;
    var img = new Image();
    img.src = StoreState[index];
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    img.onload = function () { ctx.drawImage(img, 0, 0); }
  }
}



