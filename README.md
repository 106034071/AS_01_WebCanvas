# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | Y         |


---

### How to use 
![](https://i.imgur.com/Ztsf4xZ.png)

    調色盤，可先由左方長條狀選初步顏色，再由右邊方形中選更精準的顏色
![](https://i.imgur.com/RsyjSVm.png)

    可以調整粗細、字體、字形大小的選單，有部分字體不支援中文輸入
![](https://i.imgur.com/6FfGeaz.png)

    此按鈕為一般畫筆功能，按下去就能正常畫畫，預設顏色是黑色
![](https://i.imgur.com/Q8jxnVB.png)

    這是橡皮擦按鈕，可在畫筆粗細的欄位調整大小

![](https://i.imgur.com/etwLCMU.png)

    按下之後須在畫布隨意位置點一下，就可以在該位置打字輸入
![](https://i.imgur.com/hGaza66.png) ![](https://i.imgur.com/WmRt8m4.png) ![](https://i.imgur.com/JQYR3zH.png)![](https://i.imgur.com/pbPxhCZ.png) 

    上面色個分別可以畫出圓形、矩形、三角形、直線，
![](https://i.imgur.com/pnR5shQ.png) ![](https://i.imgur.com/H39Cf4v.png)

    左邊為下載畫布到電腦，右邊為上傳圖片到畫布
![](https://i.imgur.com/pLb63W0.png)

    清空畫布
![](https://i.imgur.com/6tniu3R.png) ![](https://i.imgur.com/GIsoXJZ.png)

    左邊為Undo，右邊為Redo
    


### Function description
![](https://i.imgur.com/CJWX2Vl.png)

    按下此按紐時畫筆的顏色會隨著鼠標位置而改變，再按一次就會變回黑色(預設)
    在畫幾何圖形時，此功能也適用

### Gitlab page link

    https://106034071.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>
